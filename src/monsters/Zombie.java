package monsters;

/**
 * Creates a new type of monster and gives it all the variable values it needs.
 * @author Christian Heina
 */
public class Zombie extends Monster{
	
	/**
	 * Creates a new Zombie with all the values needed.
	 */
	public Zombie(){
		hp = 100;
		lowerDamage = 3;
		upperDamage = 5;
		lowerExp = 38;
		upperExp = 43;
		lowerGold = 40;
		upperGold = 47;
		name = "Zombie";
		monsterString = "Braiiiiiiiinnnnnnns!!";
	}

}
