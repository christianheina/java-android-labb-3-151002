package monsters;

import main.RandomHelper;

/**
 * An abstract class that implements IMonster.
 * It contains all the method logic needed to work but needs the variable values to work.
 * @author Christian Heina
 */
public abstract class Monster implements IMonster {
	public int hp, lowerDamage, upperDamage, lowerExp, upperExp, lowerGold, upperGold;
	public String name, monsterString;

	@Override
	public int attack() {
		return RandomHelper.getInt(lowerDamage,upperDamage);
	}

	@Override
	public void takeDamge(int damage) {
		hp -= damage;	
	}

	@Override
	public boolean isDead() {
		if (hp>0){
			return false;
		}else{
			return true;
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getHp() {
		return hp;
	}

	@Override
	public int getExp() {
		return RandomHelper.getInt(lowerExp, upperExp);
	}
	
	public int getGold(){
		return RandomHelper.getInt(lowerGold, upperGold);
	}
	
	/**
	 * Randomly picks a number between 1 and 100.
	 * The number decides what type of monster will be returned.
	 * @return an IMonster.
	 */
	public static IMonster getRandomMonster(){
		int pick = RandomHelper.getInt(0, 99);
		if(pick < 40){
			return new Zombie();
		}else if(pick < 65){
			return new GiantSpider();
		}else if (pick < 90){
			return new Giant();
		}else{
			return new Vampire();
		}
	}

	@Override
	public String getMonsterString() {
		return monsterString;
	}

}
