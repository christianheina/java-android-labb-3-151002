package monsters;

/**
 * Creates a new type of monster and gives it all the variable values it needs.
 * @author Christian Heina
 */
public class Vampire extends Monster{

	/**
	 * Creates a new Vampire with all the values needed.
	 */
	public Vampire(){
		hp = 100;
		lowerDamage = 10;
		upperDamage = 12;
		lowerExp = 96;
		upperExp = 99;
		lowerGold = 96;
		upperGold = 99;
		name = "Vampire";
		monsterString = "Teeth sharpening";
	}
}
