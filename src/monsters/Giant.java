package monsters;

/**
 * Creates a new type of monster and gives it all the variable values it needs.
 * @author Christian Heina
 */
public class Giant extends Monster{
	
	/**
	 * Creates a new Giant with all the values needed.
	 */
	public Giant(){
		hp = 150;
		lowerDamage = 1;
		upperDamage = 3;
		lowerExp = 65;
		upperExp = 68;
		lowerGold = 82;
		upperGold = 85;
		name = "Giant";
		monsterString = "bonk, bonk";
	}

}
