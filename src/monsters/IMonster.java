package monsters;

/**
 * Creates all the methods needed to create working monster.
 * @author Christian Heina
 */
public interface IMonster {

	/**
	 * Tells the monster to attack.
	 * @return the damage of the attack.
	 */
	public int attack();
	
	/**
	 * Tells the monster to take damage.
	 * reducing the monster's hp.
	 * @param damage - the amount to subtract from hp
	 */
	public void takeDamge(int damage);
	
	/**
	 * Checks if this monster is dead or not.
	 * @return true if hp is 0 or less otherwise false.
	 */
	public boolean isDead();
	
	/**
	 * @return the name of the monster.
	 */
	public String getName();
	
	/**
	 * @return the amount of hp the monster have left.
	 */
	public int getHp();
	
	/**
	 * @return the amount of exp the monster should give.
	 */
	public int getExp();
	
	/**
	 * @return the amount of gold the monster should give.
	 */
	public int getGold();
	
	/**
	 * @return a String that the monster would say.
	 */
	public String getMonsterString();
}
