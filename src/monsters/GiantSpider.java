package monsters;

/**
 * Creates a new type of monster and gives it all the variable values it needs.
 * @author Christian Heina
 */
public class GiantSpider extends Monster {
	
	/**
	 * Creates a new GiantSpider with all the values needed.
	 */
	public GiantSpider(){
		hp = 50;
		lowerDamage = 6;
		upperDamage = 8;
		lowerExp = 62;
		upperExp = 69;
		lowerGold = 65;
		upperGold = 67;
		name = "Giant Spider";
		monsterString = "tip tap tip tap";
	}

}
