package main;

/**
 * A static class that lets the user add to two variables.
 * The user can also get those two variables.
 * @author Christian Heina
 */
public class Shop {
	private static int strength, toughness;

	/**
	 * Adds more strength when called.
	 * @param increase - the amount to increase strength by.
	 */
	public static void attackAmulet(int increase){
		strength += increase;
	}
	
	/**
	 * Adds more toughness when called.
	 * @param increase - the amount to increase toughness by.
	 */
	public static void defenceAmulet(int increase){
		toughness += increase;
	}
	
	/**
	 * @return the amount of strength.
	 */
	public static int getStrength(){
		return strength;
	}
	
	/**
	 * @return the amount of toughness.
	 */
	public static int getToughness(){
		return toughness;
	}
}
