package main;

import java.util.Scanner;

import monsters.IMonster;
import monsters.Monster;

/**
 * A class that controls all that happens in a text based adventure game.
 * You can go on adventures, fight monsters, buy items from a shop and see your stats.
 * @author Christian Heina
 */
public class Game {

	private boolean lostGame, wonGame;
	private Scanner sc;
	private IPlayer player;
	
	/**
	 * Creates  a new Game. Start it by calling startGame().
	 */
	public Game(){
		sc = new Scanner(System.in);
	}
	
	/**
	 * Starts the game and handles the choices from the main menu.
	 * Also checks if the player won or lost the game.
	 */
	public void startGame(){
		System.out.println("************************");
		System.out.println("* Welcome to The Game! *");
		System.out.println("************************");
		System.out.print("Enter your name: ");
		player = new Player(sc.nextLine());
		
		System.out.println();
		
		int input = -1;
		while(!wonGame && !lostGame){
			printMainMenu();
			System.out.print(">");
			input = Integer.parseInt(sc.nextLine());
			switch (input){
				case 1:
					goAdventure();
					break;
				case 2:
					goCharacter();
					break;
				case 3:
					goShop();
					break;
				case 4:
					System.out.println("Bye!");
					wonGame = true;
					lostGame = true;
					break;
				default:
					System.out.println("Invalid choice!\n");
					break;
			}
		}
		
		if (wonGame && !lostGame){
			System.out.println("Congratulations! You won The Game!");
		}
		
		if (lostGame && !wonGame){
			System.out.println("Game Over!");	
		}
		
	}
	
	/**
	 * Prints all menu options.
	 */
	private void printMainMenu(){
		System.out.println("1. Go adventuring");
		System.out.println("2. Show details about your character");
		System.out.println("3. Go to shop");
		System.out.println("4. Exit game");
	}
	
	/**
	 * First menu option - fighting monsters.
	 */
	private void goAdventure(){
		if (RandomHelper.getBigChance()){
			IMonster monster = Monster.getRandomMonster();
			battle(monster);
		}else{
			System.out.println("You see nothing but swaying grass all around you..." +
					"\n[Press enter to continue]");
			sc.nextLine();
		}
	}
	
	/**
	 * Initiates the battle between the player and the given monster.
	 * Handles all possible outcomes from the fight, i.e. monster or player dies, leveling up, getting exp or gold.
	 * @param monster - the monster to fight.
	 */
	private void battle(IMonster monster){
		int damage, exp, gold;
		System.out.println("Uh oh! A wild " + monster.getName() + " appeared!");
		
		while (!player.isDead() && !monster.isDead()){
			damage = player.attack() + Shop.getStrength();
			monster.takeDamge(damage);
			System.out.println("You hit the " + monster.getName() + ", dealing " + damage + " damage");
			
			if (monster.isDead()){
				exp = monster.getExp();
				gold = monster.getGold();
				System.out.println("You killed the " + monster.getName() + ", gaining " + 
						exp + " experience and " + gold + " gold!");
				player.giveExp(exp);
				player.giveGold(gold);
				System.out.println("You are level " + player.getLevel() + ", and you have " + 
						player.getExp() + " exp and " + player.getHp() + " hp and " + player.getGold() + " gold\n");
				if (player.getLevel() >= 10){
					wonGame = true;
				}
				break;
			}
			
			damage = monster.attack() - Shop.getToughness();
			if (damage < 0){
				damage = 0;
			}
			player.takeDamage(damage);
			System.out.println(monster.getMonsterString());
			System.out.println("The " + monster.getName() + " hit you, dealing " + damage + " damage");
			
			if (player.isDead()){
				System.out.println("You were killed by the " + monster.getName() + " :(\n");
				lostGame = true;
				break;
			}
			
			System.out.println(player.getName() + ": " + player.getHp() + "hp");
			System.out.println(monster.getName() + ": " + monster.getHp() + "hp");
			System.out.println("[Press enter to continue]");
			sc.nextLine();
		}
		
	}
	
	/**
	 * Third menu option - character information.
	 * Writes out all the information about the character.
	 */
	private void goCharacter(){
		System.out.println("**********");
		System.out.println("* Name: " + player.getName());
		System.out.println("* Level: " + player.getLevel());
		System.out.println("* Hp: " + player.getHp() + "/200");
		System.out.println("* Exp: " + player.getExp() + "/100");
		System.out.println("* Gold: " + player.getGold());
		System.out.println("* Strength: " + Shop.getStrength());
		System.out.println("* Toughness: " + Shop.getToughness());
		System.out.println("**********");
		sc.nextLine();
	}
	
	/**
	 * Fourth menu option - the shop.
	 * Writes out all the things you can do in the shop.
	 * Handles all the options in the menu.
	 */
	private void goShop(){
		System.out.println("Welcome! What do you want to buy?");
		System.out.println("You have " + player.getGold() + " gold.");
		System.out.println("*****");
		boolean stop = false;
		char input;
		while (!stop){
			System.out.println("1. Attack Amulet (+ 5 strength) - 100 gold");
			System.out.println("2. Defence Amulet (+ 2 toughness) - 100 gold");
			System.out.println("E. Exit shop");
			System.out.print(">");
			input = sc.nextLine().charAt(0);
			switch (input){
				case '1':
					if(player.getGold()>=100){
						Shop.attackAmulet(5);
						player.takeGold(100);
						System.out.println("You bought an amulet. You can feel its power.");
						System.out.println("You have " + player.getGold() + " gold.");
					}else{
						System.out.println("Not enough gold, try again later.");
					}
					break;
				case '2':
					if(player.getGold()>=100){
						Shop.defenceAmulet(2);
						player.takeGold(100);
						System.out.println("You bought an amulet. You feel warm and comfy.");
						System.out.println("You have " + player.getGold() + " gold.");
					}else{
						System.out.println("Not enough gold, try again later.");
					}
					break;
				case 'E':
					stop = true;
					break;
				default:
					System.out.println("Invalid choice!\n");
					goShop();
					break;
			}
		}
	}
}
