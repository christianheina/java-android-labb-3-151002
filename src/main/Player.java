package main;

/**
 * Creates a new player with methods that change the player.
 * The class can attack, take damage, earn a range of things and take damage/die.
 * @author Christian Heina
 */
public class Player implements IPlayer{
	private String name;
	private int hp=200, exp = 0, gold = 0, level=1;
	
	/**
	 * Creates a new player.
	 * @param name - the name of the player
	 */
	public Player(String name){
		this.name = name;
	}

	@Override
	public int attack() {
		return RandomHelper.getInt(6, 10);
	}

	@Override
	public void takeDamage(int damage) {
		hp -= damage;
	}
	
	@Override
	public void takeGold(int gold){
		this.gold -= gold;
	}

	@Override
	public void giveExp(int newExp) {
		exp += newExp;
		if(exp>=100){
			level++;
			exp -= 100;
			System.out.println("You leveled up, and you are now level " + getLevel() + "!");
		}
	}
	
	@Override
	public void giveGold(int newGold){
		gold += newGold;
	}

	@Override
	public boolean isDead() {
		if (hp>0){
			return false;
		}else{
			return true;
		}
	}

	@Override
	public int getHp() {
		return hp;
	}

	@Override
	public int getExp() {
		return exp;
	}
	
	public int getGold(){
		return gold;
	}

	@Override
	public int getLevel() {
		return level;
	}

	@Override
	public String getName() {
		return name;
	}

}
