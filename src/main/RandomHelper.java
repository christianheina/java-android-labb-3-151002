package main;

import java.util.Random;

/**
 * A static class that gives you a few useful methods for randomizing different things.
 * @author Christian Heina
 */
public class RandomHelper {
	private static Random rand = new Random();
	
	/**
	 * @return true 90% of the time and false 10% of the time.
	 */
	public static boolean getBigChance(){
		if (rand.nextDouble()<0.9){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * @return true or false 50% of the time.
	 */
	public static boolean get50Chance(){
		return rand.nextBoolean();
	}
	
	/**
	 * Takes two numbers and give back a number between (including the numbers give) those.
	 * @param lower - lowest number possible.
	 * @param upper - higest number possible.
	 * @return a number between lower and upper (including these two numbers).
	 */
	public static int getInt(int lower, int upper){
		int span = 1+(upper-lower);
		return lower + rand.nextInt(span);
	}

}
