package main;

/**
 * Creates all the methods needed to create a working player class.
 * @author Christian Heina
 */
public interface IPlayer{
	
	/**
	 * Tells the player to attack.
	 * @returns the damage of the attack
	 */
	public int attack();
	
	/**
	 * tells the player to take damage, reducing the hp of the player.
	 * @param damage - amount of hp points to subtract from hp.
	 */
	public void takeDamage(int damage);
	
	/**
	 * Removes gold from the player, reducing the overall gold the player have.
	 * @param gold - amount of gold to subtract from players gold.
	 */
	public void takeGold(int gold);
	
	/**
	 * Gives the player more exp.
	 * May result in the player leveling up.
	 * @param newExp - the amount of exp to add to the player.
	 */
	public void giveExp(int newExp);
	
	/**
	 * Gives the player some gold.
	 * @param newGold - the amount of gold to add to the players overall gold.
	 */
	public void giveGold(int newGold);
	
	/**
	 * Checks if the player is dead or not.
	 * @return true if the hp of the player is 0 or less otherwise false.
	 */
	public boolean isDead();
	
	/**
	 * @return the amount of hp the player have left.
	 */
	public int getHp();
	
	/**
	 * @return the amount of exp the player have.
	 */
	public int getExp();
	
	/**
	 * @return the amount of gold the player have.
	 */
	public int getGold();
	
	/**
	 * @return the level of the player.
	 */
	public int getLevel();
	
	/**
	 * @return the name of the player.
	 */
	public String getName();
}
